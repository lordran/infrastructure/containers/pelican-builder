FROM debian:stable

WORKDIR /opt/pelican

RUN apt-get update -qq && \
    apt-get install -y git python3 python3-pip && \
    pip3 install pelican Markdown typogrify

ONBUILD RUN pelican -s publishconf.py
